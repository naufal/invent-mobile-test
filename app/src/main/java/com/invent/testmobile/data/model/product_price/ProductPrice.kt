package com.invent.testmobile.data.model.product_price

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "product_price")
class ProductPrice {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    var id: String = ""

    @SerializedName("kode_barang")
    @ColumnInfo(name = "kodeBarangSv")
    var code: String? = ""

    @SerializedName("nama_barang")
    @ColumnInfo(name = "namaBarangSv")
    var name: String? = ""

    @SerializedName("harga_barang")
    @ColumnInfo(name = "hargaBarang")
    var price: Double? = 0.0

    @SerializedName("cabang")
    @ColumnInfo(name = "cabang")
    var branch: String? = ""

}