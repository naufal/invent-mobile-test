package com.invent.testmobile.data.db

import androidx.room.*
import com.invent.testmobile.data.model.product_price.ProductPrice
import org.jetbrains.annotations.NotNull

@Dao
interface ProductPriceDao {

    @NotNull
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllProductPrice(productPrice: ProductPrice): Long

    @Delete
    fun deleteProductPrice(product: ProductPrice): Int

    @Query("SELECT * from product_price")
    fun selectAllProductPrice(): MutableList<ProductPrice>

    @Query("DELETE FROM product_price")
    fun deleteAllProductPrice()

    @Query("SELECT * from product_price WHERE product_price.kodeBarangSv = :kodeBarangKu")
    fun selectAllProductPrice(kodeBarangKu: String): MutableList<ProductPrice>

}