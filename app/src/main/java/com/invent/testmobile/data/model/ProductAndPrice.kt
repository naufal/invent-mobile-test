package com.invent.testmobile.data.model

import androidx.room.ColumnInfo

class ProductAndPrice {

    @ColumnInfo(name = "kodeBarang")
    var code: String = ""

    @ColumnInfo(name = "namaBarang")
    var name: String? = ""

    @ColumnInfo(name = "image")
    var image: String? = ""

    @ColumnInfo(name = "hargaBarang")
    var price: Double? = 0.0

    @ColumnInfo(name = "cabang")
    var branch: String? = ""

}