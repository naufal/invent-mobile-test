package com.invent.testmobile.data.repository

import com.invent.testmobile.core.MobileTestApp
import com.invent.testmobile.data.db.AppDatabase
import com.invent.testmobile.data.model.ProductAndPrice
import com.invent.testmobile.data.model.product.Product
import com.invent.testmobile.data.model.product.ProductDto
import com.invent.testmobile.data.model.product_price.ProductPrice
import com.invent.testmobile.data.model.product_price.ProductPriceDto
import com.invent.testmobile.data.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainRepository {

    private var apiService: ApiService? = null
    private val appDatabase: AppDatabase by lazy { AppDatabase.getInstance() }

    private var callProduct: Call<ProductDto>? = null

    init {
        apiService = MobileTestApp.apiService
    }

    fun getProduct(callback: RepositoryCallback<ProductDto>) {
        callProduct = apiService?.getProduct()
        callProduct?.enqueue(object : Callback<ProductDto> {
            override fun onFailure(call: Call<ProductDto>, t: Throwable?) {
                callback.onError("Something Wrong")
            }

            override fun onResponse(call: Call<ProductDto>, response: Response<ProductDto>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        if (it.results.size > 0) {
                            saveProductInDB(it.results)
                        }
                        callback.onSuccess(it)
                    }
                } else {
                    response.errorBody()?.let {
                        callback.onError(it.string())
                    }
                }
            }
        })
    }

    fun getProductPrice(callback: RepositoryCallback<ProductPriceDto>) {
        apiService?.getProductPrice()?.enqueue(object : Callback<ProductPriceDto?> {
            override fun onFailure(call: Call<ProductPriceDto?>?, t: Throwable?) {
                callback.onError("Something Wrong")
            }

            override fun onResponse(
                call: Call<ProductPriceDto?>?,
                response: Response<ProductPriceDto?>?
            ) {
                if (response != null) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            if (it.results.size > 0) {
                                saveAllProductPriceInDB(it.results)
                            }
                            callback.onSuccess(it)
                        }
                    } else {
                        response.errorBody()?.let {
                            callback.onError(it.string())
                        }
                    }
                }
            }
        })
    }

    fun saveProductInDB(results: MutableList<Product>) {
        appDatabase.productDao().deleteAllProduct()
        for (i in results) {
            appDatabase.productDao().insertProduct(i)
        }
    }

    fun saveAllProductPriceInDB(results: MutableList<ProductPrice>) {
        appDatabase.productPriceDao().deleteAllProductPrice()
        for (product in results) {
            appDatabase.productPriceDao().insertAllProductPrice(productPrice = product)
        }
    }

    fun countCabangOnProduct(kodeBarang: String): Int =
        appDatabase.productPriceDao().selectAllProductPrice(kodeBarangKu = kodeBarang).size

    fun getInfoProduct(): MutableList<Product> = appDatabase.productDao().selectAllProduct()
    fun getInfoProductPrice(kodeBarang: String): MutableList<ProductPrice> =
        appDatabase.productPriceDao().selectAllProductPrice(kodeBarangKu = kodeBarang)

    fun getInfoProductV2(): MutableList<ProductAndPrice> =
        appDatabase.productDao().selectAllProductLJPrice()

    fun getInfoProductByName(): MutableList<ProductAndPrice> =
        appDatabase.productDao().selectAllProductLJPriceByName()

    fun getInfoProductByPrice(): MutableList<ProductAndPrice> =
        appDatabase.productDao().selectAllProductLJPriceByPrice()

    interface RepositoryCallback<in T> {
        fun onSuccess(t: T?)
        fun onError(error: String)
    }

    fun deAttachAllRequest() {
        callProduct?.cancel()
    }

}