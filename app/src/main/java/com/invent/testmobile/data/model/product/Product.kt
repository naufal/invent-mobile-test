package com.invent.testmobile.data.model.product

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "product")
class Product {

    @PrimaryKey
    @SerializedName("kode_barang")
    @ColumnInfo(name = "kodeBarang")
    var code: String = ""

    @SerializedName("nama_barang")
    @ColumnInfo(name = "namaBarang")
    var name: String? = ""

    @SerializedName("image")
    @ColumnInfo(name = "image")
    var image: String? = ""

}