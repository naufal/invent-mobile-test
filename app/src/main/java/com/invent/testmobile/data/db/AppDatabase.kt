package com.invent.testmobile.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.invent.testmobile.core.MobileTestApp
import com.invent.testmobile.data.model.product.Product
import com.invent.testmobile.data.model.product_price.ProductPrice

@Database(entities = [Product::class, ProductPrice::class], version = AppDatabase.VERSION, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao
    abstract fun productPriceDao(): ProductPriceDao

    companion object {
        private const val DB_NAME = "product.db"
        const val VERSION = 6
        private val instance: AppDatabase by lazy { create(MobileTestApp.instance) }

        @Synchronized
        internal fun getInstance(): AppDatabase {
            return instance
        }

        private fun create(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }

    }

}

