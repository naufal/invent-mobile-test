package com.invent.testmobile.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.invent.testmobile.data.model.ProductAndPrice
import com.invent.testmobile.data.model.product.Product
import org.jetbrains.annotations.NotNull

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(product: Product): Long

    @NotNull
    @Query("SELECT * from product")
    fun selectAllProduct(): MutableList<Product>

    @NotNull
    @Query("SELECT namaBarang, kodeBarang, hargaBarang, cabang, image from product LEFT JOIN product_price ON product_price.kodeBarangSv = product.kodeBarang")
    fun selectAllProductLJPrice(): MutableList<ProductAndPrice>

    @NotNull
    @Query("SELECT namaBarang, kodeBarang, hargaBarang, cabang, image from product LEFT JOIN product_price ON product_price.kodeBarangSv = product.kodeBarang GROUP BY product.namaBarang")
    fun selectAllProductLJPriceByName(): MutableList<ProductAndPrice>

    @NotNull
    @Query("SELECT namaBarang, kodeBarang, hargaBarang, cabang, image from product LEFT JOIN product_price ON product_price.kodeBarangSv = product.kodeBarang GROUP BY product_price.hargaBarang")
    fun selectAllProductLJPriceByPrice(): MutableList<ProductAndPrice>

    @Query("DELETE FROM product")
    fun deleteAllProduct()

}