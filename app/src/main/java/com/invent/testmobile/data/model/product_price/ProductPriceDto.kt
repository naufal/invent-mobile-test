package com.invent.testmobile.data.model.product_price

import com.google.gson.annotations.SerializedName

data class ProductPriceDto (

    @SerializedName("status_code")
    val statusCode: String,

    @SerializedName("status_message_ind")
    val statusMessageInd: String,

    @SerializedName("status_message_eng")
    val statusMessageEng: String,

    @SerializedName("value")
    val results: MutableList<ProductPrice>

)