package com.invent.testmobile.data.network

import com.invent.testmobile.data.model.product.ProductDto
import com.invent.testmobile.data.model.product_price.ProductPriceDto
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("test_core/v1/get_m_product")
    fun getProduct(): Call<ProductDto>

    @GET("test_core/v1/get_product_price")
    fun getProductPrice(): Call<ProductPriceDto>

}