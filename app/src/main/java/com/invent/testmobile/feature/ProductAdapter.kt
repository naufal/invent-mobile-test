package com.invent.testmobile.feature

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.invent.testmobile.R
import com.invent.testmobile.data.model.ProductAndPrice
import com.invent.testmobile.util.BaseHelper
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_item.view.*

class ProductAdapter(val onDataExistListener: OnDataExistListener?) : RecyclerView.Adapter<ProductAdapter.SimpleHolder>() {

    private var items: MutableList<ProductAndPrice> = arrayListOf()
    private var itemsSaved: MutableList<ProductAndPrice> = arrayListOf()

    interface OnDataChangedListener {
        fun onDataChanged()
    }

    interface OnDataExistListener {
        fun onDataExist(isExist: Boolean)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SimpleHolder, position: Int) {
        holder.bind(items[position], object : OnDataChangedListener {
            override fun onDataChanged() {
                notifyDataSetChanged()
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleHolder =
        SimpleHolder(LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false))

    class SimpleHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(productfinal: ProductAndPrice, onDataChangedListener: OnDataChangedListener) = with(productfinal) {
            Picasso.with(itemView.context).load(image).into(itemView.ivProduct)
            itemView.tvProductName.text = name

            val hargaProduct = if (price == null) "0" else price.toString()
            itemView.tvInfoCabang.visibility = View.VISIBLE
            itemView.tvInfoCabang.text = "Harga Rp. "+BaseHelper.formatCurrencyDouble(itemView.context, hargaProduct)
            if (branch != null) {
                itemView.tvInfoCabang.append("\nCabang $branch")
            }
        }
    }

    fun add(list: MutableList<ProductAndPrice>) {
        items.addAll(list)
        itemsSaved.addAll(list)
        notifyDataSetChanged()
    }

    fun clear() {
        items.clear()
        itemsSaved.clear()
        notifyDataSetChanged()
    }

    fun filterList(query: String?) {
        query?.let { it ->
            if (it.isEmpty()) {
                items = itemsSaved
                notifyDataSetChanged()
            } else {
                items = itemsSaved.filter {
                    it.name!!.contains(query.toString()) || it.price.toString().contains(query.toString())
                } as MutableList<ProductAndPrice>

                notifyDataSetChanged()
            }
            if (items.isNotEmpty()) {
                onDataExistListener?.onDataExist(true)
            } else {
                onDataExistListener?.onDataExist(false)
            }
        }
    }

}