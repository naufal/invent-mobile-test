package com.invent.testmobile.feature

import com.invent.testmobile.data.model.ProductAndPrice
import com.invent.testmobile.data.model.product.ProductDto

interface HomeContract {

    interface View  {
        fun showWait()
        fun removeWait()
        fun showProduct(fullInfoProduct: MutableList<ProductAndPrice>)
        fun onFailure(message: String)
    }

    interface Presenter{
        fun getProduct()
        fun getProductPrice(productDto: ProductDto)
        fun getProductByName()
        fun getProductByHarga()
        fun getProductOffline()
        fun attachView(view: View)
        fun detachView()
    }

}