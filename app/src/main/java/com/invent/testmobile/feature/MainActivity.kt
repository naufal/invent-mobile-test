package com.invent.testmobile.feature

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.invent.testmobile.R
import com.invent.testmobile.data.model.ProductAndPrice
import com.invent.testmobile.data.repository.MainRepository
import com.invent.testmobile.util.BaseHelper
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), HomeContract.View, ProductAdapter.OnDataExistListener {

    private var mainPresenter: MainPresenter? = null
    private var menuSaved: Menu? = null

    var adapter : ProductAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initPresenter()
        assignListener()
    }

    private fun initPresenter() {
        mainPresenter = MainPresenter(MainRepository())
        mainPresenter?.attachView(this)

        if (BaseHelper.isOnline(this)) {
            mainPresenter?.getProduct()
        } else {
            mainPresenter?.getProductOffline()
        }
    }

    private fun assignListener() {
        swipeRefresh?.setOnRefreshListener {
            if (BaseHelper.isOnline(this)) {
                mainPresenter?.getProduct()
            } else {
                mainPresenter?.getProductOffline()
            }
        }
    }

    override fun showWait() {
        //todo shimmer?
        swipeRefresh?.isRefreshing = true
    }

    override fun removeWait() {
        swipeRefresh?.isRefreshing = false
    }

    override fun showProduct(fullInfoProduct: MutableList<ProductAndPrice>) {
        swipeRefresh?.isRefreshing = false
        adapter = ProductAdapter(this)
        rvProduct.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        rvProduct.adapter = adapter
        if (fullInfoProduct.isNotEmpty()) {
            adapter?.add(fullInfoProduct)
            tvProductNotExist.text = ""
        } else {
            tvProductNotExist.text = getString(R.string.product_not_exist)
        }
    }

    override fun onFailure(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menuSaved = menu
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchItem: MenuItem = menu.findItem(R.id.search)

        val searchView: SearchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                adapter?.filterList(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter?.filterList(newText)
                return false
            }

        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sort -> {
                //todo sort list option
                val menuItemView: View = findViewById(R.id.sort)
                val popupMenu = PopupMenu(this, menuItemView)
                popupMenu.menuInflater.inflate(R.menu.menu_sort, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { items ->
                    when (items.itemId) {
                        R.id.namaProduk ->
                            mainPresenter?.getProductByName()
                        R.id.hargaProduk ->
                            mainPresenter?.getProductByHarga()
                    }
                    true
                }
                popupMenu.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDataExist(isExist: Boolean) {
        if (!isExist) tvProductNotExist.text = getString(R.string.product_not_exist) else tvProductNotExist.text = ""
    }

}