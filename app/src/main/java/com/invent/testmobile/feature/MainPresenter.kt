package com.invent.testmobile.feature

import com.invent.testmobile.data.model.product.ProductDto
import com.invent.testmobile.data.model.product_price.ProductPriceDto
import com.invent.testmobile.data.repository.MainRepository

class MainPresenter (private val mainRepository: MainRepository) : HomeContract.Presenter{

    var homeView: HomeContract.View? = null

    override fun getProduct() {
        homeView?.showWait()
        mainRepository.getProduct(object : MainRepository.RepositoryCallback<ProductDto> {
            override fun onSuccess(t: ProductDto?) {
                t?.let {
                    getProductPrice(it)
                }
            }

            override fun onError(error: String) {
                homeView?.removeWait()
                homeView?.onFailure(error)
            }
        })
    }

    //TODO get Product Price https://invent-integrasi.com/test_core/v1/get_product_price
    override fun getProductPrice(productDto: ProductDto) {
        homeView?.showWait()
        mainRepository.getProductPrice(object : MainRepository.RepositoryCallback<ProductPriceDto> {
            override fun onSuccess(t: ProductPriceDto?) {
                t?.let {
                    homeView?.showProduct(mainRepository.getInfoProductV2())
                    homeView?.removeWait()
                }
            }

            override fun onError(error: String) {
                homeView?.removeWait()
                homeView?.onFailure(error)
            }
        })
    }

    override fun getProductOffline() {
        homeView?.showProduct(mainRepository.getInfoProductV2())
    }

    override fun getProductByName() {
        homeView?.showProduct(mainRepository.getInfoProductByName())
    }

    override fun getProductByHarga() {
        homeView?.showProduct(mainRepository.getInfoProductByPrice())
    }

    override fun attachView(view: HomeContract.View) {
        homeView = view
    }

    override fun detachView() {
        mainRepository.deAttachAllRequest()
        homeView = null
    }

}
