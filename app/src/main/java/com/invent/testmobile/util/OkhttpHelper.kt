package com.invent.testmobile.util

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object OkhttpHelper {

    fun getOkHttpsM3(): OkHttpClient {
        //todo ssl custom trust
        // Install the custom trust manager
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return try {
            OkHttpClient.Builder()
                .sslSocketFactory(CustomTrust.initCustomTrust().sslSocketFactory(), CustomTrust.initCustomTrust().trustManager)
                .certificatePinner(CustomTrust.certificatePinner)
                .callTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build()
        } catch (e: Exception) {
            OkHttpClient()
        }
    }

}