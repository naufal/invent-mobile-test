package com.invent.testmobile.util

import okhttp3.CertificatePinner
import okhttp3.tls.HandshakeCertificates
import okhttp3.tls.decodeCertificatePem
import java.security.cert.X509Certificate
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

//TODO CUSTOM TRUST need Java
object CustomTrust {
    ///todo Main Trusted Subject
    //todo Trusted 1
    //todo Trusted 2
    val certificatePinner: CertificatePinner
        get() = CertificatePinner.Builder() ///todo Main Trusted Subject
            .add(
                "invent-integrasi.com",
                "sha256/OHLGr+sR1qpTboF1qYWsENjgWV7ordxzYfAw9p83S90="
            ) //todo Trusted 1
            .add(
                "invent-integrasi.com",
                "sha256/x4QzPSC810K5/cMjb05Qm4k3Bw5zBn4lTdO/nEW/Td4="
            ) //todo Trusted 2
            .add("invent-integrasi.com", "sha256/vRU+17BDT2iGsXvOi76E7TQMcTLXAqj0+jGPdW7L1vM=")
            .build()

    fun trustAll(): Array<TrustManager> {
        return arrayOf(
            object : X509TrustManager {
                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {}
                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {}
                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            }
        )
    }

    val customCAFromPEM: X509Certificate
        get() = """-----BEGIN CERTIFICATE-----
MIIF0DCCBLigAwIBAgIQWyfVZTMYDtacP7jGa2TLvzANBgkqhkiG9w0BAQsFADCB
jzELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4G
A1UEBxMHU2FsZm9yZDEYMBYGA1UEChMPU2VjdGlnbyBMaW1pdGVkMTcwNQYDVQQD
Ey5TZWN0aWdvIFJTQSBEb21haW4gVmFsaWRhdGlvbiBTZWN1cmUgU2VydmVyIENB
MB4XDTIwMDUyOTAwMDAwMFoXDTIxMDUzMDIzNTk1OVowHzEdMBsGA1UEAxMUaW52
ZW50LWludGVncmFzaS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
AQCjHLgzYosqbni8zOUxPSD7/Bco1ONkBcOQbIEcd+0oraASeM9+fMpFBQy+cHNk
f06JvsJ4XONJIg0QXEAE1UqtYuz1cELTaZfQcwDQCtMNMz6wIQ9ZvPSeql2ate8n
qQ/F6ExvoK948uE/l+dsItyZzS6JBwFcOh//CfgPegZZA2K+avCZ6+zAAPQ+/neQ
4uBAmRudOVRU5AkOe5McZ86osLMF244pO9x0/bdFddJGXjK78hU+uF6TeJ6dexpR
Z6WHe7rB5feCT5/nB+AFUYwpU8IcwoXJR/RveyVsP2Rn0l2JqFKatgbG4Zbv1B6B
GteP961+8afhoSAVHq9kTDwPAgMBAAGjggKVMIICkTAfBgNVHSMEGDAWgBSNjF7E
VK2K4Xfpm/mbBeG4AY1h4TAdBgNVHQ4EFgQUx3h5hhieA8eJ/7rNwvdLsevRlosw
DgYDVR0PAQH/BAQDAgWgMAwGA1UdEwEB/wQCMAAwHQYDVR0lBBYwFAYIKwYBBQUH
AwEGCCsGAQUFBwMCMEkGA1UdIARCMEAwNAYLKwYBBAGyMQECAgcwJTAjBggrBgEF
BQcCARYXaHR0cHM6Ly9zZWN0aWdvLmNvbS9DUFMwCAYGZ4EMAQIBMIGEBggrBgEF
BQcBAQR4MHYwTwYIKwYBBQUHMAKGQ2h0dHA6Ly9jcnQuc2VjdGlnby5jb20vU2Vj
dGlnb1JTQURvbWFpblZhbGlkYXRpb25TZWN1cmVTZXJ2ZXJDQS5jcnQwIwYIKwYB
BQUHMAGGF2h0dHA6Ly9vY3NwLnNlY3RpZ28uY29tMDkGA1UdEQQyMDCCFGludmVu
dC1pbnRlZ3Jhc2kuY29tghh3d3cuaW52ZW50LWludGVncmFzaS5jb20wggEDBgor
BgEEAdZ5AgQCBIH0BIHxAO8AdQB9PvL4j/+IVWgkwsDKnlKJeSvFDngJfy5ql2iZ
fiLw1wAAAXJhOmMQAAAEAwBGMEQCIDmIOcsVVWH2r2szIjDQzqYP1CQXMUQ/X8j2
eJEI8mWQAiBzqHUnrfSq+jQ/fzPRaAFouAzwZmEGjQkLqAcXDgI9rAB2AJQgvB6O
1Y1siHMfgosiLA3R2k1ebE+UPWHbTi9YTaLCAAABcmE6YzkAAAQDAEcwRQIgBOKv
SZx5fAmizu4IFqzVxknSWPf9XaioR74X3srk82UCIQDkHd6amwlLQhYiPZlc5sQS
UZZ3j5oppe1Lv909XZ8PajANBgkqhkiG9w0BAQsFAAOCAQEAWuZKuX/ztMjtOpoQ
hZ1JjxqRJlGhJHK+h2c9TV8mBgUbm6S1oC75MWo+fYwwS1k5bgubW32v3mYQNdbi
muor9OjIjU2l8yXgb+reVjOoMVRkX78U4/XdbUMU9No6pArhgsQW6ARlb0OtU0qQ
UgpKtehFAgMEq6nCoKyXdKgxcOobNCZhZcrLGjyc+lIJ9PWQe9FaDSZQyV2pgCm4
091uQ7CwX/CpvxbKFMfdsWClT5/t3l1WPHAZGdv3avzXcO3r5iqJ+JNq5CaZ0Ym7
z8M9+7Ig7XQhwsqjJOnNcbiqNEPg/SVwVO8Cm3wIZkN/cTa/rtm2GrC2Y9ueobp1
do2Lcw==
-----END CERTIFICATE-----""".decodeCertificatePem()

    fun initCustomTrust(): HandshakeCertificates {
        return HandshakeCertificates.Builder()
            .addTrustedCertificate(customCAFromPEM) // Uncomment if standard certificates are also required.
            //.addPlatformTrustedCertificates()
            .build()
    }
}