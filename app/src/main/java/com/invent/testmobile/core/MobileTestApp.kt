package com.invent.testmobile.core

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.invent.testmobile.data.network.ApiService
import com.invent.testmobile.util.CustomTrust
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates


class MobileTestApp : Application() {

    companion object {
        @JvmStatic
        var apiService: ApiService? = null
        var instance: MobileTestApp by Delegates.notNull()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .sslSocketFactory(CustomTrust.initCustomTrust().sslSocketFactory(), CustomTrust.initCustomTrust().trustManager)
            .certificatePinner(CustomTrust.certificatePinner)
            .callTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(Config.REMOTE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        apiService = retrofit.create(ApiService::class.java)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}